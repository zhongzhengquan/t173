class SchoolUpdater

  attr_reader :data_source

  def initialize school_source
    @data_source = school_source
    @added_class_numbers = 0
    @added_student_numbers = 0
  end

  def execute
    update_school
    SyncActivity.create start_date: data_source.sync_start_time,
                        end_date: data_source.sync_end_time,
                        added_class_numbers: @added_class_numbers,
                        added_student_numbers: @added_student_numbers

  end

  private
  def update_school
    data_source.classes.map do |clazz|
      saved_clazz = Clazz.where(class_id: clazz['classId']).first
      unless saved_clazz
        @added_class_numbers += 1
        saved_clazz = Clazz.create class_id: clazz['classId'],
                                   class_name: clazz['className'],
                                   semester: clazz['semester'],
                                   start_date: clazz['startDate'],
                                   end_date: clazz['endDate'],
                                   class_teacher: clazz['classTeacher'],
                                   class_room: clazz['classRoom'],
                                   numbers: clazz['numbers']
      end
      clazz['students'].each do |student|
        next if saved_clazz.students.where(student_id: student['studentId']).first
        @added_student_numbers =+ 1
        saved_clazz.students.create student_id: student['studentId'],
                                    student_name: student['studentName'],
                                    company: student['company'],
                                    phone_number: student['phoneNumber'],
                                    id_number: student['IDNumber'],
                                    erp_number: student['ERPNumber']
      end
    end
  end
end
