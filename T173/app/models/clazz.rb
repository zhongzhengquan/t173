class Clazz
  include Mongoid::Document
  include Mongoid::Timestamps

  store_in collection: 'classes'
  has_many :students

  field :class_id, type: String
  field :class_name, type: String
  # 开班期数
  field :semester, type: String
  field :start_date, type: String
  field :end_date, type: String
  # 班主任
  field :class_teacher, type: String
  field :class_room, type: String
  field :numbers, type: Integer
end
