### 开发环境
    # 安装mongodb数据库
    $ brew install mongodb

    # 启动mongodb数据库
    $ mongod --dbpath /opt/db/tmp

    # 启动开发环境
    $ ruby boot.rb

    # 调试
    $ pry

### 产品环境

使用docker作为产品环境

#### 配置数据库

    $ docker pull tutum/mongodb
    # 注意： 修改mypass 与 config/mongoid.yml 中的密码一致
    $ docker run -p 27017:27017 -p 28017:28017 -e MONGODB_USER="admin" -e MONGODB_DATABASE="schools" -e MONGODB_PASS="mypass"  --name=mongo tutum/mongodb


#### 构建docker镜像

    $ docker build -t web .
    # 注意: 这里需要链接已经启动的mongodb 数据库, db作为数据库容器的别名， 和mconfig/mongoid.yml配置一致
    $ docker run -P --name web --link mongo:db web

#### 统一启动

    $ docker-compose up
