class User

  attr_accessor :id
  attr_accessor :username
  attr_accessor :nickname
  attr_accessor :email
  attr_accessor :phone_number
  attr_reader :organization_ids

  def initialize
    @organization_ids = []
  end

  def add_organization_id id_or_ids
    @organization_ids.concat Array(id_or_ids)
  end
end
