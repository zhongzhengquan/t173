require 'json'
require 'spreadsheet'
Spreadsheet.client_encoding = 'UTF-8'

class SheetReader

  attr_accessor :content

  attr_accessor :path

  def initialize path
    @path = path
    @content = Spreadsheet.open @path
  end

  def worksheet
    @default_worksheet ||= @content.worksheet(0)
  end

  def read
    Worksheet2json.new.convert(worksheet, TicketFormat::VALUE.keys)
  end
end
